output "DynamoDB" {
  value = "Nombre Table ${aws_dynamodb_table.http-habi-dynamodb.name}"
}
output "Role-Lamba" {
  value = "Rol ARN:${aws_iam_role.rol-lamba.arn}"
}
output "Lambda" {
  value = "Lamba ${aws_lambda_function.http-habi-lambda.function_name}" 
}
output "http-habi-api" {
  value = "La IP ${aws_apigatewayv2_api.habi-api.api_endpoint}"
}