resource "aws_dynamodb_table" "http-habi-dynamodb" {
  name = "habi-dynamo"
  read_capacity  = 10
  write_capacity = 10
  hash_key = "id"
  attribute {
    name = "id"
    type = "S"
  }
}

resource "aws_iam_role" "rol-lamba" {
  name = "HabiLambdaRol"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_iam_policy" "habi-rol-api" {
  name = "habiPolicyRol"
  policy = jsonencode (
    {
      "Version" : "2012-10-17",
      "Statement" : [{
      "Effect" : "Allow",
      "Action" : [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:GetLogEvents",
        "logs:FilterLogEvents",
        "logs:PutLogEvents"
      ],
      "Resource" : ["arn:aws:logs:*:*:*" 
      ]
      },
      {
      "Effect" : "Allow",
      "Action" : [
        "dynamodb:DeleteItem",
        "dynamodb:GetItem",
        "dynamodb:PutItem",
        "dynamodb:Scan",
        "dynamodb:UpdateItem"
      ],
      "Resource" : ["${aws_dynamodb_table.http-habi-dynamodb.arn}"]
      }
      ]
    })
}

resource "aws_iam_role_policy_attachment" "habi-policy-to-role" {
  policy_arn = aws_iam_policy.habi-rol-api.arn
  role = aws_iam_role.rol-lamba.name
}

data "archive_file" "lambda" {
  type = "zip"
  source_file = "index.js"
  output_path = "funtion.zip"
}

resource "aws_lambda_function" "http-habi-lambda" {
  filename = "funtion.zip"
  function_name = "http-habi-lambda"
  role = aws_iam_role.rol-lamba.arn
  handler = "index.js"
  runtime = "nodejs16.x"
  #environment {
  #  variables =  {
  #    foo = "bar"
  #  }
  #}
}

resource "aws_apigatewayv2_api" "habi-api" {
  name          = "http-habi-api"
  protocol_type = "HTTP"
}

resource "time_sleep" "wait_30_seconds" {
  create_duration = "40s"
}

resource "aws_apigatewayv2_route" "api-ruta-items-id-put" {
  depends_on = [time_sleep.wait_30_seconds]
  api_id    = aws_apigatewayv2_api.habi-api.id
  route_key = "PUT /items"
  target = "integrations/${aws_apigatewayv2_integration.lambda.id}"
  authorization_type = "AWS_IAM"
}

resource "aws_apigatewayv2_route" "api-ruta-items-get" {
  api_id    = aws_apigatewayv2_api.habi-api.id
  route_key = "GET /items"
  target = "integrations/${aws_apigatewayv2_integration.lambda.id}"
  authorization_type = "AWS_IAM"
}

resource "aws_apigatewayv2_route" "api-ruta-items-id-get" {
  api_id    = aws_apigatewayv2_api.habi-api.id
  route_key = "GET /items/{id}"
  target = "integrations/${aws_apigatewayv2_integration.lambda.id}"
  authorization_type = "AWS_IAM"
}

resource "aws_apigatewayv2_route" "api-ruta-items-id-delete" {
  api_id    = aws_apigatewayv2_api.habi-api.id
  route_key = "DELETE /items/{id}"
  target = "integrations/${aws_apigatewayv2_integration.lambda.id}"
  authorization_type = "AWS_IAM"
}

resource "aws_apigatewayv2_integration" "lambda" {
  api_id = aws_apigatewayv2_api.habi-api.id
  integration_uri = aws_lambda_function.http-habi-lambda.invoke_arn
  integration_type = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_lambda_permission" "permisos-api" {
  statement_id = "AllowExecutionFromAPIGateway"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.http-habi-lambda.function_name
  principal = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.habi-api.execution_arn}/*/*/items/{id}"
}

resource "aws_cloudwatch_log_group" "habi-api-logs" {
  name = "apiLogsApi"

  tags = {
    Environment = "habi"
    Application = "API-Lambda"
  }
}


resource "aws_apigatewayv2_stage" "habi-stage-lambda" {  
api_id = aws_apigatewayv2_api.habi-api.id
name        = "$default"  
auto_deploy = true
access_log_settings {    
destination_arn = aws_cloudwatch_log_group.habi-api-logs.arn
    	format = jsonencode({      
    		requestId               = "$context.requestId"      
    		sourceIp                = "$context.identity.sourceIp"      
    		requestTime             = "$context.requestTime"      
    		protocol                = "$context.protocol"      
    		httpMethod              = "$context.httpMethod"      
    		resourcePath            = "$context.resourcePath"      
    		routeKey                = "$context.routeKey"      
    		status                  = "$context.status"      
    		responseLength          = "$context.responseLength"      
    		integrationErrorMessage = "$context.integrationErrorMessage"
    		})
    	}
}