#Con estado remoto en gitlab
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.56.0"
    }
  }
  backend "http" {
  address = "https://gitlab.com/api/v4/projects/46738455/terraform/state/default"
  lock_address = "https://gitlab.com/api/v4/projects/46738455/terraform/state/default/lock"
  unlock_address = "https://gitlab.com/api/v4/projects/46738455/terraform/state/default/lock"
  }
}
# Sin profile
provider "aws" {
  region = "us-east-1"
}




