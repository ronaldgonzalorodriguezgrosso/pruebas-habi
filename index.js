import { createRequire } from 'module';
const require = createRequire(import.meta.url);

const AWS = require("aws-sdk");

const dynamo = new DynamoDB.DocumentClient();

export async function handler(event, context) {
  let body;
  let statusCode = 200;
  const headers = {
	"Content-Type": "application/json"
  };

  try {
	switch (event.routeKey) {
  	case "DELETE /items/{id}":
    	await dynamo
      	.delete({
        	TableName: "habi-dynamo",
        	Key: {
          	id: event.pathParameters.id
        	}
      	})
      	.promise();
    	body = `Deleted item ${event.pathParameters.id}`;
    	break;
  	case "GET /items/{id}":
    	body = await dynamo
      	.get({
        	TableName: "habi-dynamo",
        	Key: {
          	id: event.pathParameters.id
        	}
      	})
      	.promise();
    	break;
  	case "GET /items":
    	body = await dynamo.scan({ TableName: "habi-dynamo" }).promise();
    	break;
  	case "PUT /items":
    	let requestJSON = JSON.parse(event.body);
    	await dynamo
      	.put({
        	TableName: "habi-dynamo",
        	Item: {
          	id: requestJSON.id,
          	price: requestJSON.price,
          	name: requestJSON.name
        	}
      	})
      	.promise();
    	body = `Put item ${requestJSON.id}`;
    	break;
  	default:
    	throw new Error(`Unsupported route: "${event.routeKey}"`);
	}
  } catch (err) {
	statusCode = 400;
	body = err.message;
  } finally {
	body = JSON.stringify(body);
  }

  return {
	statusCode,
	body,
	headers
  };
}